package de.tkunkel.synology.entry;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.tkunkel.synology.common.Config;
import de.tkunkel.synology.common.SynologyAction;
import de.tkunkel.synology.common.SynologyWrapper;
import de.tkunkel.synology.common.data.ActiveSessions;
import de.tkunkel.synology.common.data.ConnectionsList;
import de.tkunkel.synology.common.data.Result;
import de.tkunkel.synology.common.exceptions.SynologyIOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EntryRequest extends SynologyWrapper {
    private static Logger LOG = LogManager.getLogger(EntryRequest.class);
    private ObjectMapper mapper = new ObjectMapper(); // create once, reuse

    public EntryRequest(Config config) {
        super(config);
        this.config = config;
    }

    public ConnectionsList listNetworkDevices() throws SynologyIOException, IOException {
        LOG.entry();
        String responseString = executeCommand(SynologyAction.ENTRY_REQUEST);
        LOG.debug(responseString);

        validateSuccess(responseString);
        JsonNode readTree = mapper.readTree(responseString);
        JsonNode data = readTree.get("data");
        ConnectionsList connections = mapper.treeToValue(data, ConnectionsList.class);
        LOG.debug(connections);

        LOG.exit();

        return connections;
    }

    public ActiveSessions listActiveSessions() throws SynologyIOException, IOException {
        LOG.entry();

        String responseString = executeCommand(SynologyAction.LIST_ACTIVE_SESSIONS);
        JsonNode readTree = mapper.readTree(responseString);
        LOG.debug("responseString=" + responseString);
        JsonNode data = readTree.get("data");
        ActiveSessions activeSessions = mapper.treeToValue(data, ActiveSessions.class);
        LOG.debug("activeSessions=" + activeSessions);

        LOG.exit();
        return activeSessions;
    }

    public NetworkInfo getInfoForType(NetworkType type) throws SynologyIOException, IOException {
        LOG.entry();
        NetworkInfo rc = null;
        String responseString = executeCommand(SynologyAction.ENTRY_REQUEST);
        LOG.debug(responseString);

        validateSuccess(responseString);
        JsonNode readTree = mapper.readTree(responseString);
        JsonNode data = readTree.get("data");
        ConnectionsList connections = mapper.treeToValue(data, ConnectionsList.class);

        for (Result result : connections.getResult()) {
            if (result.getApi().equals("SYNO.Entry.Request")) {
                rc = processResult(result, type);
            }
        }

        LOG.debug(connections);

        LOG.exit();

        return rc;
    }

    @SuppressWarnings("unchecked")
    private NetworkInfo processResult(Result result, NetworkType type) {
        NetworkInfo networkInfo = null;
        //System.out.println(result.getData().getAdditionalProperties().get("result"));
        //System.out.println(result.getData().getAdditionalProperties().get("has_fail"));
        ArrayList<HashMap> resultList = (ArrayList<HashMap>) result.getData().getAdditionalProperties().get("result");
        Boolean resultHasFail = Boolean.valueOf(String.valueOf(result.getData().getAdditionalProperties().get("has_fail")));

        for (HashMap map : resultList) {
            if (map != null && map.get("api") != null && map.get("api").equals(type.getApi())) {
                networkInfo = new NetworkInfo();
                networkInfo.setType(type);
                networkInfo.setSuccess(Boolean.valueOf(String.valueOf(map.get("success"))));
                networkInfo.setMethod(String.valueOf(map.get("method")));
                networkInfo.setVersion(Integer.valueOf(String.valueOf(map.get("version"))));
                ArrayList<HashMap<String, String>> data = (ArrayList<HashMap<String, String>>) map.get("data");
                if (data != null && data.size() > 0) {
                    for (Map.Entry<String, String> entry : data.get(0).entrySet()) {
                        networkInfo.getData().getData().put(entry.getKey(), entry.getValue());
                    }
                    //TODO what to do with entries > 1 ?
                }
                //System.out.println(map);
            } else {
                //System.out.println(map.get("api")+" =?= "+type.getApi());
            }
        }
        return networkInfo;
    }
}
