package de.tkunkel.synology.common.data;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "error",
        "error_desc",
        "id",
        "link_status",
        "path",
        "sess_id",
        "share",
        "status",
        "task_display_name",
        "task_name",
        "type",
        "type_id",
        "unfinished_files",
        "user_id"
})
public class Connection {

    @JsonProperty("error")
    private Integer error;
    @JsonProperty("error_desc")
    private String errorDesc;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("link_status")
    private Integer linkStatus;
    @JsonProperty("path")
    private String path;
    @JsonProperty("sess_id")
    private Integer sessId;
    @JsonProperty("share")
    private String share;
    @JsonProperty("status")
    private String status;
    @JsonProperty("task_display_name")
    private String taskDisplayName;
    @JsonProperty("task_name")
    private String taskName;
    @JsonProperty("type")
    private String type;
    @JsonProperty("type_id")
    private Integer typeId;
    @JsonProperty("unfinished_files")
    private String unfinishedFiles;
    @JsonProperty("user_id")
    private String userId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The error
     */
    @JsonProperty("error")
    public Integer getError() {
        return error;
    }

    /**
     * @param error The error
     */
    @JsonProperty("error")
    public void setError(Integer error) {
        this.error = error;
    }

    /**
     * @return The errorDesc
     */
    @JsonProperty("error_desc")
    public String getErrorDesc() {
        return errorDesc;
    }

    /**
     * @param errorDesc The error_desc
     */
    @JsonProperty("error_desc")
    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The linkStatus
     */
    @JsonProperty("link_status")
    public Integer getLinkStatus() {
        return linkStatus;
    }

    /**
     * @param linkStatus The link_status
     */
    @JsonProperty("link_status")
    public void setLinkStatus(Integer linkStatus) {
        this.linkStatus = linkStatus;
    }

    /**
     * @return The path
     */
    @JsonProperty("path")
    public String getPath() {
        return path;
    }

    /**
     * @param path The path
     */
    @JsonProperty("path")
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return The sessId
     */
    @JsonProperty("sess_id")
    public Integer getSessId() {
        return sessId;
    }

    /**
     * @param sessId The sess_id
     */
    @JsonProperty("sess_id")
    public void setSessId(Integer sessId) {
        this.sessId = sessId;
    }

    /**
     * @return The share
     */
    @JsonProperty("share")
    public String getShare() {
        return share;
    }

    /**
     * @param share The share
     */
    @JsonProperty("share")
    public void setShare(String share) {
        this.share = share;
    }

    /**
     * @return The status
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The taskDisplayName
     */
    @JsonProperty("task_display_name")
    public String getTaskDisplayName() {
        return taskDisplayName;
    }

    /**
     * @param taskDisplayName The task_display_name
     */
    @JsonProperty("task_display_name")
    public void setTaskDisplayName(String taskDisplayName) {
        this.taskDisplayName = taskDisplayName;
    }

    /**
     * @return The taskName
     */
    @JsonProperty("task_name")
    public String getTaskName() {
        return taskName;
    }

    /**
     * @param taskName The task_name
     */
    @JsonProperty("task_name")
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    /**
     * @return The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The typeId
     */
    @JsonProperty("type_id")
    public Integer getTypeId() {
        return typeId;
    }

    /**
     * @param typeId The type_id
     */
    @JsonProperty("type_id")
    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    /**
     * @return The unfinishedFiles
     */
    @JsonProperty("unfinished_files")
    public String getUnfinishedFiles() {
        return unfinishedFiles;
    }

    /**
     * @param unfinishedFiles The unfinished_files
     */
    @JsonProperty("unfinished_files")
    public void setUnfinishedFiles(String unfinishedFiles) {
        this.unfinishedFiles = unfinishedFiles;
    }

    /**
     * @return The userId
     */
    @JsonProperty("user_id")
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    @JsonProperty("user_id")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(error).append(errorDesc).append(id).append(linkStatus).append(path).append(sessId).append(share).append(status).append(taskDisplayName).append(taskName).append(type).append(typeId).append(unfinishedFiles).append(userId).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Connection) == false) {
            return false;
        }
        Connection rhs = ((Connection) other);
        return new EqualsBuilder().append(error, rhs.error).append(errorDesc, rhs.errorDesc).append(id, rhs.id).append(linkStatus, rhs.linkStatus).append(path, rhs.path).append(sessId, rhs.sessId).append(share, rhs.share).append(status, rhs.status).append(taskDisplayName, rhs.taskDisplayName).append(taskName, rhs.taskName).append(type, rhs.type).append(typeId, rhs.typeId).append(unfinishedFiles, rhs.unfinishedFiles).append(userId, rhs.userId).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
