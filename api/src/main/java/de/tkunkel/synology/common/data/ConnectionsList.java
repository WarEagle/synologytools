package de.tkunkel.synology.common.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "has_fail",
        "result"
})
public class ConnectionsList {

    @JsonProperty("has_fail")
    private Boolean hasFail;
    @JsonProperty("result")
    private List<Result> result = new ArrayList<Result>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The hasFail
     */
    @JsonProperty("has_fail")
    public Boolean getHasFail() {
        return hasFail;
    }

    /**
     *
     * @param hasFail
     * The has_fail
     */
    @JsonProperty("has_fail")
    public void setHasFail(Boolean hasFail) {
        this.hasFail = hasFail;
    }

    /**
     *
     * @return
     * The result
     */
    @JsonProperty("result")
    public List<Result> getResult() {
        return result;
    }

    /**
     *
     * @param result
     * The result
     */
    @JsonProperty("result")
    public void setResult(List<Result> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(hasFail).append(result).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ConnectionsList) == false) {
            return false;
        }
        ConnectionsList rhs = ((ConnectionsList) other);
        return new EqualsBuilder().append(hasFail, rhs.hasFail).append(result, rhs.result).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}