package de.tkunkel.synology.common.data;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "cipherkey",
        "ciphertoken",
        "public_key",
        "server_time"
})
public class LoginKey {

    @JsonProperty("cipherkey")
    private String cipherkey;
    @JsonProperty("ciphertoken")
    private String ciphertoken;
    @JsonProperty("public_key")
    private String publicKey;
    @JsonProperty("server_time")
    private Integer serverTime;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The cipherkey
     */
    @JsonProperty("cipherkey")
    public String getCipherkey() {
        return cipherkey;
    }

    /**
     * @param cipherkey The cipherkey
     */
    @JsonProperty("cipherkey")
    public void setCipherkey(String cipherkey) {
        this.cipherkey = cipherkey;
    }

    /**
     * @return The ciphertoken
     */
    @JsonProperty("ciphertoken")
    public String getCiphertoken() {
        return ciphertoken;
    }

    /**
     * @param ciphertoken The ciphertoken
     */
    @JsonProperty("ciphertoken")
    public void setCiphertoken(String ciphertoken) {
        this.ciphertoken = ciphertoken;
    }

    /**
     * @return The publicKey
     */
    @JsonProperty("public_key")
    public String getPublicKey() {
        return publicKey;
    }

    /**
     * @param publicKey The public_key
     */
    @JsonProperty("public_key")
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    /**
     * @return The serverTime
     */
    @JsonProperty("server_time")
    public Integer getServerTime() {
        return serverTime;
    }

    /**
     * @param serverTime The server_time
     */
    @JsonProperty("server_time")
    public void setServerTime(Integer serverTime) {
        this.serverTime = serverTime;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(cipherkey).append(ciphertoken).append(publicKey).append(serverTime).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof LoginKey) == false) {
            return false;
        }
        LoginKey rhs = ((LoginKey) other);
        return new EqualsBuilder().append(cipherkey, rhs.cipherkey).append(ciphertoken, rhs.ciphertoken).append(publicKey, rhs.publicKey).append(serverTime, rhs.serverTime).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}