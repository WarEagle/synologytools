package de.tkunkel.synology.common.data;


import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "repeat",
        "shuffle"
})
/*
generated with http://www.jsonschema2pojo.org/
 */
public class PlayMode {

    @JsonProperty("repeat")
    private String repeat;
    @JsonProperty("shuffle")
    private Boolean shuffle;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The repeat
     */
    @JsonProperty("repeat")
    public String getRepeat() {
        return repeat;
    }

    /**
     * @param repeat The repeat
     */
    @JsonProperty("repeat")
    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    /**
     * @return The shuffle
     */
    @JsonProperty("shuffle")
    public Boolean getShuffle() {
        return shuffle;
    }

    /**
     * @param shuffle The shuffle
     */
    @JsonProperty("shuffle")
    public void setShuffle(Boolean shuffle) {
        this.shuffle = shuffle;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(repeat).append(shuffle).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PlayMode) == false) {
            return false;
        }
        PlayMode rhs = ((PlayMode) other);
        return new EqualsBuilder().append(repeat, rhs.repeat).append(shuffle, rhs.shuffle).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}