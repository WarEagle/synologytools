package de.tkunkel.synology.common;

public enum HttpRequestMode {
    POST, GET
}
