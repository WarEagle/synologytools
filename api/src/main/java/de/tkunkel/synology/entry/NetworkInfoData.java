package de.tkunkel.synology.entry;

import java.util.HashMap;

public class NetworkInfoData {
    private final HashMap<String, String> data = new HashMap<>();

    public HashMap<String, String> getData() {
        return data;
    }
}
