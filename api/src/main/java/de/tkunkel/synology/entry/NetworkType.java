package de.tkunkel.synology.entry;

public enum NetworkType {
    SYNO_Core_CMS_Info("SYNO.Core.CMS.Info"),
    SYNO_Core_Network_Wifi_Client("SYNO.Core.Network.Wifi.Client"),
    SYNO_Core_Network_Bond("SYNO.Core.Network.Bond"),
    SYNO_Core_Network_Bridge("SYNO.Core.Network.Bridge"),
    SYNO_Core_Network_Ethernet("SYNO.Core.Network.Ethernet"),
    SYNO_Core_Network_LocalBridge("SYNO.Core.Network.LocalBridge"),
    SYNO_Core_Network_USBModem("SYNO.Core.Network.USBModem"),
    SYNO_Core_Network_PPPoE("SYNO.Core.Network.PPPoE"),
    SYNO_Core_Network_IPv6Tunnel("SYNO.Core.Network.IPv6Tunnel"),
    SYNO_Core_Network_VPN_PPTP("SYNO.Core.Network.VPN.PPTP"),
    SYNO_Core_Network_VPN_OpenVPN("SYNO.Core.Network.VPN.OpenVPN"),
    SYNO_Core_Network_VPN_L2TP("SYNO.Core.Network.VPN.L2TP"),
    SYNO_Core_Network_Router_Topology("SYNO.Core.Network.Router.Topology"),;

    private final String api;

    NetworkType(String api) {
        this.api = api;
    }

    public String getApi() {
        return api;
    }

    public static NetworkType fromString(String typeAsString) {
        for (NetworkType networkType : NetworkType.values()) {
            if (networkType.getApi().equalsIgnoreCase(typeAsString)) {
                return networkType;
            }
        }
        return null;
    }
}
