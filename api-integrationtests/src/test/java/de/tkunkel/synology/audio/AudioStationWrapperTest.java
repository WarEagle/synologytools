package de.tkunkel.synology.audio;

import de.tkunkel.synology.LocalSynologyTestbase;
import de.tkunkel.synology.common.data.SongList;
import de.tkunkel.synology.common.SessionManager;
import de.tkunkel.synology.common.exceptions.SynologyIOException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class AudioStationWrapperTest extends LocalSynologyTestbase {

    @Before
    public void init(){
        SessionManager.createInstance(configAudiostation);
    }

    @Test
    public void testCallTestGetCurrentPlayList() throws SynologyIOException, IOException {
        AudioStationWrapper audioStationWrapper = new AudioStationWrapper(configAudiostation);
        audioStationWrapper.getCurrentPlayList(SongSortBy.RANDOM);
        audioStationWrapper.getCurrentPlayList(SongSortBy.ASCENDING);
        audioStationWrapper.getCurrentPlayList(SongSortBy.DESCENDING);
    }

    @Test
    public void testCallTestSongList() throws SynologyIOException, IOException {
        AudioStationWrapper audioStationWrapper = new AudioStationWrapper(configAudiostation);
        audioStationWrapper.getSongList(1, "shared", SongSortBy.DESCENDING);
        audioStationWrapper.getSongList(10, "shared", SongSortBy.DESCENDING);
        audioStationWrapper.getSongList(100, "shared", SongSortBy.DESCENDING);

        audioStationWrapper.getSongList(10, "shared", SongSortBy.DESCENDING);
        audioStationWrapper.getSongList(10, "shared", SongSortBy.ASCENDING);
        audioStationWrapper.getSongList(10, "shared", SongSortBy.RANDOM);
    }

    @Test
    public void testAddSongToPlaylist() throws SynologyIOException, IOException {
        AudioStationWrapper audioStationWrapper = new AudioStationWrapper(configAudiostation);
        clearCurrentPlaylist(audioStationWrapper);
        addRandomSongToCurrentPlaylist(audioStationWrapper);

        SongList songList = audioStationWrapper.getCurrentPlayList(SongSortBy.ASCENDING);
        Assert.assertNotNull(songList);
        Assert.assertNotNull(songList.getSongs());
        Assert.assertEquals(1, songList.getSongs().size());

        clearCurrentPlaylist(audioStationWrapper);
    }

    @Test
    public void testGetAndSetVolume() throws SynologyIOException, IOException {
        AudioStationWrapper audioStationWrapper = new AudioStationWrapper(configAudiostation);
        audioStationWrapper.setVolume(0);
        float volume = audioStationWrapper.getVolume();
        Assert.assertEquals(0, volume, 0);

        audioStationWrapper.setVolume(1);
        volume = audioStationWrapper.getVolume();
        Assert.assertEquals(1, volume, 0);

        audioStationWrapper.setVolume(-1);
        volume = audioStationWrapper.getVolume();
        Assert.assertEquals(0, volume, 0);
    }

    private void addRandomSongToCurrentPlaylist(AudioStationWrapper audioStationWrapper) throws SynologyIOException, IOException {
        SongList list = audioStationWrapper.getSongList(1, "shared", SongSortBy.RANDOM);
        audioStationWrapper.addToPlaylist("shared", list.getSongs().get(0).getId());
    }

    private void clearCurrentPlaylist(AudioStationWrapper audioStationWrapper) throws SynologyIOException, IOException {
        SongList songList = audioStationWrapper.getCurrentPlayList(SongSortBy.ASCENDING);
        audioStationWrapper.clearPlaylist(songList.getSongs().size());
    }

}