package de.tkunkel.synology.common;

public class Config {
    private String host;
    private String protocol;
    private int port;
    private String username;
    private String password;

    public Config(String username, String password, String protocol, String host, int port) {
        this.host = host;
        this.port = port;
        this.protocol = protocol;
        this.username = username;
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
