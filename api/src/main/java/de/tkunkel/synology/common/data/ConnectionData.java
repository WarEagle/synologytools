package de.tkunkel.synology.common.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "conn",
        "is_pause",
        "notification",
        "total",
        "tray_status"
})
public class ConnectionData {

    @JsonProperty("conn")
    private List<Connection> conn = new ArrayList<Connection>();
    @JsonProperty("is_pause")
    private Boolean isPause;
    @JsonProperty("notification")
    private Notification notification;
    @JsonProperty("total")
    private Integer total;
    @JsonProperty("tray_status")
    private String trayStatus;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The conn
     */
    @JsonProperty("conn")
    public List<Connection> getConn() {
        return conn;
    }

    /**
     *
     * @param conn
     * The conn
     */
    @JsonProperty("conn")
    public void setConn(List<Connection> conn) {
        this.conn = conn;
    }

    /**
     *
     * @return
     * The isPause
     */
    @JsonProperty("is_pause")
    public Boolean getIsPause() {
        return isPause;
    }

    /**
     *
     * @param isPause
     * The is_pause
     */
    @JsonProperty("is_pause")
    public void setIsPause(Boolean isPause) {
        this.isPause = isPause;
    }

    /**
     *
     * @return
     * The notification
     */
    @JsonProperty("notification")
    public Notification getNotification() {
        return notification;
    }

    /**
     *
     * @param notification
     * The notification
     */
    @JsonProperty("notification")
    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    /**
     *
     * @return
     * The total
     */
    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    /**
     *
     * @param total
     * The total
     */
    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     *
     * @return
     * The trayStatus
     */
    @JsonProperty("tray_status")
    public String getTrayStatus() {
        return trayStatus;
    }

    /**
     *
     * @param trayStatus
     * The tray_status
     */
    @JsonProperty("tray_status")
    public void setTrayStatus(String trayStatus) {
        this.trayStatus = trayStatus;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(conn).append(isPause).append(notification).append(total).append(trayStatus).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ConnectionData) == false) {
            return false;
        }
        ConnectionData rhs = ((ConnectionData) other);
        return new EqualsBuilder().append(conn, rhs.conn).append(isPause, rhs.isPause).append(notification, rhs.notification).append(total, rhs.total).append(trayStatus, rhs.trayStatus).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}