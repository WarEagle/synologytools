package de.tkunkel.synology.common.data;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "file_count",
        "latest_file_info"
})
public class Notification {

    @JsonProperty("file_count")
    private Integer fileCount;
    @JsonProperty("latest_file_info")
    private LatestFileInfo latestFileInfo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The fileCount
     */
    @JsonProperty("file_count")
    public Integer getFileCount() {
        return fileCount;
    }

    /**
     *
     * @param fileCount
     * The file_count
     */
    @JsonProperty("file_count")
    public void setFileCount(Integer fileCount) {
        this.fileCount = fileCount;
    }

    /**
     *
     * @return
     * The latestFileInfo
     */
    @JsonProperty("latest_file_info")
    public LatestFileInfo getLatestFileInfo() {
        return latestFileInfo;
    }

    /**
     *
     * @param latestFileInfo
     * The latest_file_info
     */
    @JsonProperty("latest_file_info")
    public void setLatestFileInfo(LatestFileInfo latestFileInfo) {
        this.latestFileInfo = latestFileInfo;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(fileCount).append(latestFileInfo).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Notification) == false) {
            return false;
        }
        Notification rhs = ((Notification) other);
        return new EqualsBuilder().append(fileCount, rhs.fileCount).append(latestFileInfo, rhs.latestFileInfo).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
