package de.tkunkel.synology.core;

import de.tkunkel.synology.LocalSynologyTestbase;
import de.tkunkel.synology.common.data.ProcessInfo;
import de.tkunkel.synology.common.SessionManager;
import de.tkunkel.synology.common.exceptions.SynologyIOException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class CoreSystemTest extends LocalSynologyTestbase {

    @Test
    public void callTestProcesses() throws SynologyIOException, IOException {
        SessionManager.createInstance(configSystem);
        CoreSystem coreSystem = new CoreSystem(configSystem);
        ProcessInfo processInfo = coreSystem.processInfo();
        Assert.assertNotNull(processInfo);
        Assert.assertNotNull(processInfo.getProcess());
    }

}