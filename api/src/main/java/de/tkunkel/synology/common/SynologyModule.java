package de.tkunkel.synology.common;

public enum SynologyModule {

    AUTH("Auth", "SYNO.API.Auth", "/webapi/auth.cgi", 3),
    ENCRYPTION("Encrypt", "SYNO.API.Encryption", "/webapi/encryption.cgi", 1),
    LOGIN("Login", null, "/webapi/login.cgi", 1),
    LOGOUT("Logout", null, "/webman/logout.cgi", SynologyModule.VERSION_INVALID),
    CORE_SYSTEM("CoreSystem", "SYNO.Core.System", "/webapi/_______________________________________________________entry.cgi", 1),
    CORE_CURRENT_CONNECTION("Entry", "SYNO.Core.CurrentConnection", "/webapi/_______________________________________________________entry.cgi", 1),
    AUDIOSTATION_REMOTEPLAYER("AudioStation", "SYNO.AudioStation.RemotePlayer", "/webapi/AudioStation/remote_player.cgi", 2),
    AUDIOSTATION_REMOTEPLAYER_STATUS("AudioStation", "SYNO.AudioStation.RemotePlayerStatus", "/webapi/AudioStation/remote_player_status.cgi", 1),
    AUDIOSTATION_SONG_LIST("AudioStation", "SYNO.AudioStation.Song", "/webapi/AudioStation/song.cgi", 2),
    ENTRY_REQUEST("Entry", "SYNO.Entry.Request", "/webapi/_______________________________________________________entry.cgi", 1),
    ;

    public final static int VERSION_INVALID=-999;
    private String moduleName;
    private String api;
    private String urlPart;
    private int version;

    /**
     * All parameters are based on the http-request, if you need to add new entries, just trace your request and add the corresponding parameters
     * You can get additional information on on APIs at http://diskstation:5000/webapi/query.cgi?api=SYNO.API.Info&version=1&method=query&query=all
     * adjust the hostname and port to your environment.
     * @param moduleName
     * @param api
     * @param urlPart The URL without any parameters and without hostinformation
     * @param version
     */
    SynologyModule(String moduleName, String api, String urlPart, int version) {
        this.moduleName = moduleName;
        this.urlPart = urlPart;
        this.api = api;
        this.version = version;
    }

    public String getModuleName() {
        return moduleName;
    }

    public String getApi() {
        return api;
    }

    public int getVersion() {
        return version;
    }

    public String getUrlPart() {
        return urlPart;
    }
}
