package de.tkunkel.synology.core;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.tkunkel.synology.common.data.ProcessInfo;
import de.tkunkel.synology.common.Config;
import de.tkunkel.synology.common.SynologyAction;
import de.tkunkel.synology.common.SynologyWrapper;
import de.tkunkel.synology.common.exceptions.SynologyIOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class CoreSystem extends SynologyWrapper {
    private static Logger LOG = LogManager.getLogger(CoreSystem.class);
    private ObjectMapper mapper = new ObjectMapper(); // create once, reuse

    public CoreSystem(Config config) {
        super(config);
        this.config = config;
    }

    /**
     * Requests the list of currently active processes.
     */
    public ProcessInfo processInfo() throws SynologyIOException, IOException {
        LOG.entry();
        String responseString = executeCommand(SynologyAction.CORE_SYSTEM_PROCESS_INFO);
        LOG.debug(responseString);

        validateSuccess(responseString);
        JsonNode readTree = mapper.readTree(responseString);
        JsonNode data = readTree.get("data");
        ProcessInfo processInfo = mapper.treeToValue(data, ProcessInfo.class);
        LOG.debug(processInfo);

        LOG.exit();

        return processInfo;
    }

}
