package de.tkunkel.synology.common;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum SynologyAction {
    AUDIOSTATION_STOP(SynologyModule.AUDIOSTATION_REMOTEPLAYER
            , HttpRequestMode.POST
            , new BasicNameValuePair("action", "stop")
            , new BasicNameValuePair("method", "control")
    ),
    AUDIOSTATION_PLAY(SynologyModule.AUDIOSTATION_REMOTEPLAYER
            , HttpRequestMode.POST
            , new BasicNameValuePair("action", "play")
            , new BasicNameValuePair("method", "control")
    ),
    AUDIOSTATION_UPDATE_PLAYLIST(SynologyModule.AUDIOSTATION_REMOTEPLAYER
            , HttpRequestMode.POST
            , new BasicNameValuePair("action", "play")
            , new BasicNameValuePair("method", "updateplaylist")
            , new BasicNameValuePair("offset", "0")
            , new BasicNameValuePair("limit", "0")
            , new BasicNameValuePair("play", "true")
    ),
    AUDIOSTATION_GET_PLAYLIST(SynologyModule.AUDIOSTATION_REMOTEPLAYER
            , HttpRequestMode.POST
            , new BasicNameValuePair("method", "getplaylist")
            , new BasicNameValuePair("offset", "0")
            , new BasicNameValuePair("limit", "8192")
            , new BasicNameValuePair("sort_by", "")
            , new BasicNameValuePair("additional", "song_tag%2Csong_audio%2Csong_rating")
    ),
    AUDIOSTATION_CLEAR_PLAYLIST(SynologyModule.AUDIOSTATION_REMOTEPLAYER
            , HttpRequestMode.POST
            , new BasicNameValuePair("action", "play")
            , new BasicNameValuePair("method", "updateplaylist")
            , new BasicNameValuePair("offset", "0")
            , new BasicNameValuePair("songs", "")
            , new BasicNameValuePair("updated_index", "-1")
    ),
    AUDIOSTATION_PAUSE(SynologyModule.AUDIOSTATION_REMOTEPLAYER
            , HttpRequestMode.POST
            , new BasicNameValuePair("action", "pause")
            , new BasicNameValuePair("method", "control")
    ),
    AUDIOSTATION_NEXT(SynologyModule.AUDIOSTATION_REMOTEPLAYER
            , HttpRequestMode.POST
            , new BasicNameValuePair("action", "next")
            , new BasicNameValuePair("method", "control")
    ),
    AUDIOSTATION_SET_VOLUME(SynologyModule.AUDIOSTATION_REMOTEPLAYER
            , HttpRequestMode.POST
            , new BasicNameValuePair("action", "set_volume")
            , new BasicNameValuePair("method", "control")
    ),
    AUDIOSTATION_GET_VOLUME(SynologyModule.AUDIOSTATION_REMOTEPLAYER_STATUS
            , HttpRequestMode.GET
            , new BasicNameValuePair("method", "getStatus")
            , new BasicNameValuePair("additional", "subplayer_volume")
    ),
    AUDIOSTATION_PREVIOUS(SynologyModule.AUDIOSTATION_REMOTEPLAYER
            , HttpRequestMode.POST
            , new BasicNameValuePair("action", "prev")
            , new BasicNameValuePair("method", "control")
    ),
    AUDIOSTATION_STATUS(SynologyModule.AUDIOSTATION_REMOTEPLAYER_STATUS
            , HttpRequestMode.GET
            , new BasicNameValuePair("method", "getStatus")
            , new BasicNameValuePair("additional", "song_tag%2Csong_audio%2Csubplayer_volume")
    ),
    AUDIOSTATION_SONGLIST(SynologyModule.AUDIOSTATION_SONG_LIST
            , HttpRequestMode.POST
            , new BasicNameValuePair("method", "list")
            , new BasicNameValuePair("additional", "song_tag%2Csong_audio%2Csong_rating")
            , new BasicNameValuePair("sort_by", "title")
    ),
    AUTH_LOGIN(SynologyModule.AUTH
            , HttpRequestMode.GET
            , new BasicNameValuePair("method", "login")
            , new BasicNameValuePair("format", "cookie")
    ),
    AUTH_LOGOUT(SynologyModule.LOGOUT
            , HttpRequestMode.GET
    ),
    ENCRYPTION_GET_KEY(SynologyModule.ENCRYPTION
            , HttpRequestMode.POST
            , new BasicNameValuePair("method", "getinfo")
            , new BasicNameValuePair("format", "module")
    ),
    LOGIN_LOGIN(SynologyModule.LOGIN
            , HttpRequestMode.POST
            , new BasicNameValuePair("method", "getinfo")
            , new BasicNameValuePair("format", "module")
            , new BasicNameValuePair("Cookie", "stay_login=1")
    ),
    CORE_SYSTEM_PROCESS_INFO(SynologyModule.CORE_SYSTEM
            , HttpRequestMode.POST
            //the " are there on purpose, they are in the real request executed from the webpage
            , new BasicNameValuePair("type", "\"process\"")
            , new BasicNameValuePair("method", "info")
    ),
    ENTRY_REQUEST(SynologyModule.ENTRY_REQUEST
            , HttpRequestMode.POST
            , new BasicNameValuePair("method", "request")
            , new BasicNameValuePair("stop_when_error", "false")
            , new BasicNameValuePair("compound", "[{\"api\":\"SYNO.Entry.Request\",\"method\":\"request\",\"version\":1,\"stopwhenerror\":false,\"compound\":[{\"api\":\"SYNO.Core.CMS.Info\",\"method\":\"get\",\"version\":1,\"additional\":[\"gluster_role\"]},{\"api\":\"SYNO.Core.Network.Wifi.Client\",\"method\":\"list\",\"version\":1},{\"api\":\"SYNO.Core.Network.Bond\",\"method\":\"list\",\"version\":1},{\"api\":\"SYNO.Core.Network.Bridge\",\"method\":\"list\",\"version\":1},{\"api\":\"SYNO.Core.Network.Ethernet\",\"method\":\"list\",\"version\":1},{\"api\":\"SYNO.Core.Network.LocalBridge\",\"method\":\"list\",\"version\":1,\"bridge_type\":[\"local_bridge\"]},{\"api\":\"SYNO.Core.Network.USBModem\",\"method\":\"list\",\"version\":1},{\"api\":\"SYNO.Core.Network.PPPoE\",\"method\":\"list\",\"version\":1},{\"api\":\"SYNO.Core.Network.IPv6Tunnel\",\"method\":\"get\",\"version\":1},{\"api\":\"SYNO.Core.Network.VPN.PPTP\",\"method\":\"list\",\"version\":1,\"additional\":[\"status\"]},{\"api\":\"SYNO.Core.Network.VPN.OpenVPN\",\"method\":\"list\",\"version\":1,\"additional\":[\"status\"]},{\"api\":\"SYNO.Core.Network.VPN.L2TP\",\"method\":\"list\",\"version\":1,\"additional\":[\"status\"]},{\"api\":\"SYNO.Core.Network.Router.Topology\",\"method\":\"get\",\"version\":2}]},{\"api\":\"SYNO.CloudSync\",\"method\":\"list_conn\",\"version\":1,\"is_tray\":true}]")
    ),
    LIST_ACTIVE_SESSIONS(SynologyModule.CORE_CURRENT_CONNECTION
            , HttpRequestMode.POST
            , new BasicNameValuePair("sort_by", "time")
            , new BasicNameValuePair("offset", "0")
            , new BasicNameValuePair("limit", "50")
            , new BasicNameValuePair("start", "0")
            , new BasicNameValuePair("action", "\"enum\"")
            , new BasicNameValuePair("sort_direction", "DESC")
            , new BasicNameValuePair("method", "list")
    ),
    ;

    private final SynologyModule module;
    private final HttpRequestMode httpRequestMode;
    private final List<NameValuePair> parameters = new ArrayList<>();

    SynologyAction(SynologyModule module, HttpRequestMode httpRequestMode, NameValuePair... parameters) {
        this.module = module;
        this.httpRequestMode = httpRequestMode;
        Collections.addAll(this.parameters, parameters);
    }

    public List<NameValuePair> getParameters() {
        List<NameValuePair> tmp = new ArrayList<>(parameters);
        if (module.getVersion() != SynologyModule.VERSION_INVALID) {
            tmp.add(new BasicNameValuePair("version", String.valueOf(module.getVersion())));
            //TODO What does this id mean? Is it based on the synology-firmware-version?
            tmp.add(new BasicNameValuePair("id", "0005CD5F782B"));
        }
        return tmp;
    }

    public SynologyModule getModule() {
        return module;
    }

    public HttpRequestMode getHttpRequestMode() {
        return httpRequestMode;
    }
}
