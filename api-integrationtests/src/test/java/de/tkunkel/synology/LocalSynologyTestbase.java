package de.tkunkel.synology;

import de.tkunkel.synology.common.Config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Classes extending from this class are running against a real synology diskstation, so don't run the tests while you are listening to music.
 */
public abstract class LocalSynologyTestbase {
    /*
        The logindata is put into properties and there is only one of them added to the repository, so my real password isn't in the repository.
    */
    InputStream isNotInGit = LocalSynologyTestbase.class.getResourceAsStream("/tkt_logindata.properties");
    InputStream is = LocalSynologyTestbase.class.getResourceAsStream("/logindata.properties");

    protected Config configAudiostation;
    protected Config configSystem;

    protected LocalSynologyTestbase() {
        try {
            Properties properties = new Properties();
            if (isNotInGit != null) {
                properties.load(isNotInGit);
            } else {
                properties.load(is);
            }

            configAudiostation = new Config(
                    String.valueOf(properties.getProperty("audiostationUsername"))
                    , String.valueOf(properties.getProperty("audiostationPassword"))
                    , String.valueOf(properties.getProperty("audiostationProtocol"))
                    , String.valueOf(properties.getProperty("audiostationHost"))
                    , Integer.valueOf(String.valueOf(properties.getProperty("audiostationPort"))));

            configSystem = new Config(
                    String.valueOf(properties.getProperty("globalUsername"))
                    , String.valueOf(properties.getProperty("globalPassword"))
                    , String.valueOf(properties.getProperty("globalProtocol"))
                    , String.valueOf(properties.getProperty("globalHost"))
                    , Integer.valueOf(String.valueOf(properties.getProperty("globalPort"))));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
