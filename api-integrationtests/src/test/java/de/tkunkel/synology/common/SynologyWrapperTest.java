package de.tkunkel.synology.common;

import de.tkunkel.synology.common.exceptions.SynologyIOException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SynologyWrapperTest  {
    private SynologyWrapper synologyWrapper ;
    private Config config;

    @Test
    public void validateSuccess_True() throws  Exception{
        synologyWrapper.validateSuccess("{\"data\":{\"sid\":\"gADuoc1CfPOZUC5KNN03654\"},\"success\":true}");
    }

    @Test(expected = Exception.class)
    public void validateSuccess_False_InvalidRest() throws  Exception{
        synologyWrapper.validateSuccess("something");
    }

    @Test(expected = Exception.class)
    public void validateSuccess_False_SucessFalse() throws  Exception{
        synologyWrapper.validateSuccess("{\"data\":{\"sid\":\"gADuoc1CfPOZUC5KNN03654\"},\"success\":false}");
    }

    @Test
    public void testGenerateBaseUrlString() {
        String baseUrlString = synologyWrapper.generateBaseUrlString(SynologyAction.AUDIOSTATION_PAUSE);
        Assert.assertNotNull(baseUrlString);
        Assert.assertEquals("HTTP://HOST:99/webapi/AudioStation/remote_player.cgi", baseUrlString);
    }

    @Before
    public void init(){
        config = new Config("user", "password", "HTTP","HOST",99);

        synologyWrapper = new SynologyWrapper(config) {
            @Override
            protected void validateSuccess(String responseString) throws SynologyIOException {
                super.validateSuccess(responseString);
            }
        };

    }
}
