package de.tkunkel.synology.audio;

public enum SongSortBy {
    /**
     * the result is sorted ascending based on the columns requested
     */
    ASCENDING("ASC"),
    /**
     * the result is sorted descending based on the columns requested
     */
    DESCENDING("DESC"),
    /**
     * The result is in random order.
     */
    RANDOM("random"),
    ;

    private final String commandName;

    SongSortBy(String commandName) {
        this.commandName = commandName;
    }

    public String getCommandName() {
        return commandName;
    }
}
