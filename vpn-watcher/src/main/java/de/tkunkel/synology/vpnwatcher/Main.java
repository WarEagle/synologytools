package de.tkunkel.synology.vpnwatcher;

import de.tkunkel.synology.common.Config;
import de.tkunkel.synology.entry.NetworkType;

import javax.swing.*;
import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

public class Main {
    private final static Image imageOk = createImage("/images/ok.png", "OK");
    private final static Image imageError = createImage("/images/error.png", "Error");
    private static Properties properties;

    public static void main(String[] args) throws IOException {
        if (args == null  || args.length != 1){
            System.err.println("please provide a configuration.property-file");
            System.exit(-1);
        }else{
            properties = new Properties();
            properties.load(new FileReader(args[0]));
        }

        /* Use an appropriate Look and Feel */
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            // UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (UnsupportedLookAndFeelException | IllegalAccessException | ClassNotFoundException | InstantiationException ex) {
            ex.printStackTrace();
        }
        /* Turn off metal's use of bold fonts */
        UIManager.put("swing.boldMetal", Boolean.FALSE);
        //Schedule a job for the event-dispatching thread: adding TrayIcon.
        SwingUtilities.invokeLater(() -> createAndShowGUI());
    }

    private static void createAndShowGUI() {
        //Check the SystemTray support
        if (!SystemTray.isSupported()) {
            System.out.println("SystemTray is not supported");
            return;
        }
        final PopupMenu popup = new PopupMenu();
        final TrayIcon trayIcon = new TrayIcon(imageOk);
        final SystemTray tray = SystemTray.getSystemTray();

        // Create a popup menu components
        MenuItem exitItem = new MenuItem("Exit");

        //Add components to popup menu
        popup.add(exitItem);

        trayIcon.setPopupMenu(popup);
        trayIcon.setImageAutoSize(true);

        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.out.println("TrayIcon could not be added.");
            return;
        }

        trayIcon.addActionListener(e -> JOptionPane.showMessageDialog(null,
                "This dialog box is run from System Tray"));

        exitItem.addActionListener(e -> {
            tray.remove(trayIcon);
            System.exit(0);
        });

        Config config = new Config(properties.getProperty("username")
                , properties.getProperty("password")
                , properties.getProperty("protocol")
                , properties.getProperty("host")
                , Integer.parseInt(properties.getProperty("port")));

        UpdateThread updateThread = new UpdateThread(config
                , trayIcon
                , imageOk
                , imageError
                , properties.getProperty("networkName")
                , NetworkType.fromString(properties.getProperty("networkType")));
        Thread updater = new Thread(updateThread);
        updater.setDaemon(true);
        updater.start();

    }

    //Obtain the image URL
    protected static Image createImage(String path, String description) {
        URL imageURL = Main.class.getResource(path);

        if (imageURL == null) {
            System.err.println("Resource not found: " + path);
            return null;
        } else {
            return (new ImageIcon(imageURL, description)).getImage();
        }
    }
}