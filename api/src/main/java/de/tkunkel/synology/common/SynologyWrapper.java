package de.tkunkel.synology.common;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.tkunkel.synology.common.exceptions.NotInitializedException;
import de.tkunkel.synology.common.exceptions.SynologyIOException;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class SynologyWrapper {
    private static Logger LOG = LogManager.getLogger(SynologyWrapper.class);
    protected Config config;
    private ObjectMapper mapper = new ObjectMapper(); // create once, reuse
    private SessionManager sessionManager;
    private CloseableHttpClient httpClient;

    protected SynologyWrapper(Config config) {
        httpClient = HttpClients.createDefault();

        try {
            sessionManager = SessionManager.getInstance();
        } catch (NotInitializedException e) {
            LOG.error(e);
        }
        this.config = config;
    }

    protected void validateSuccess(String responseString) throws SynologyIOException {
        JsonNode readTree;
        try {
            readTree = mapper.readTree(responseString);
            JsonNode error = readTree.get("error");
            if (error != null) {
                throw new SynologyIOException("Error processing '" + responseString + "'");
            }

            JsonNode success = readTree.get("success");
            if (success != null) {
                if (success.asBoolean()) {
                    LOG.debug("success");
                    return;
                } else {
                    throw new SynologyIOException("Error processing '" + responseString + "'");
                }
            }

            JsonNode data = readTree.get("data");
            if (data != null) {
                JsonNode dataSuccess = data.get("success");
                Boolean dataSuccessBoolean = Boolean.valueOf(dataSuccess.textValue());
                if (dataSuccessBoolean) {
                    LOG.debug("success");
                    return;
                }
            }
            throw new SynologyIOException("Error processing '" + responseString + "'");

        } catch (IOException e) {
            LOG.error(e);
            throw new SynologyIOException(e.getMessage());
        }
    }

    protected String executeCommand(SynologyAction action, NameValuePair... additionalParameters) throws SynologyIOException {
        LOG.entry(action);
        if (action.getHttpRequestMode() == HttpRequestMode.GET) {
            LOG.exit();
            return executeCommandGet(action, true, additionalParameters);
        } else {
            LOG.exit();
            return executeCommandPost(action, true, additionalParameters);
        }
    }

    protected String executeCommandWithoutLogin(SynologyAction action, NameValuePair... additionalParameters) throws SynologyIOException {
        LOG.entry(action);
        if (action.getHttpRequestMode() == HttpRequestMode.GET) {
            LOG.exit();
            return executeCommandGet(action, false, additionalParameters);
        } else {
            LOG.exit();
            return executeCommandPost(action, false, additionalParameters);
        }
    }

    protected String generateBaseUrlString(SynologyAction action) {
        StringBuilder rc = new StringBuilder();
        rc.append(config.getProtocol());
        rc.append("://");
        rc.append(config.getHost());
        rc.append(":");
        rc.append(config.getPort());
        rc.append(action.getModule().getUrlPart());
        return rc.toString();
    }

    /**
     * Special case for logout because in this case the sessionId must be passed through
     */
    protected void executeCommandSpecialLogout(SynologyAction action, String sessionId) throws SynologyIOException {
        StringBuilder requestUrl = new StringBuilder();
        requestUrl.append(generateBaseUrlString(action));

        LOG.debug(requestUrl);
        HttpGet httpGet = new HttpGet(requestUrl.toString());
        httpGet.addHeader("Cookie", "stay_login=1; id=" + sessionId);

        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            HttpEntity entity = response.getEntity();
            String responseString = EntityUtils.toString(entity).trim();
            LOG.debug("responseString=" + responseString);
        } catch (IOException e) {
            LOG.error(e);
            throw new SynologyIOException(e.getMessage());
        }
    }

    private String executeCommandGet(SynologyAction action, boolean performLogin, NameValuePair... additionalParameters) throws SynologyIOException {
        String rc;
        List<NameValuePair> nameValuePairs = fillParameters(action, additionalParameters);

        StringBuilder requestUrl = new StringBuilder();
        requestUrl.append(generateBaseUrlString(action));

        String querystring = URLEncodedUtils.format(nameValuePairs, "utf-8");
        if (!StringUtils.isEmpty(querystring)) {
            requestUrl.append("?");
            requestUrl.append(querystring);
        }

        LOG.debug("performLogin=" + performLogin);
        LOG.debug(requestUrl);
        LOG.debug(nameValuePairs);
        HttpGet httpGet = new HttpGet(requestUrl.toString());
        fillHeader(action, performLogin, httpGet);

        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            rc = processResponse(response);
        } catch (IOException e) {
            LOG.error(e);
            throw new SynologyIOException(e.getMessage());
        }

        return rc == null ? null : rc.trim();
    }

    private void fillHeader(SynologyAction action, boolean performLogin, HttpRequestBase request) {
        if (performLogin) {
            request.addHeader("Cookie", "stay_login=1; id=" + sessionManager.getSessionIdFor(action.getModule()));
        } else {
            request.addHeader("Cookie", "stay_login=1;");
        }
    }

    private String executeCommandPost(SynologyAction action, boolean performLogin, NameValuePair... additionalParameters) throws SynologyIOException {
        String rc;
        HttpPost httpPost = new HttpPost(generateBaseUrlString(action));
        fillHeader(action, performLogin, httpPost);

        List<NameValuePair> nameValuePairs = fillParameters(action, additionalParameters);
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        } catch (IOException e) {
            LOG.error(e);
            throw new SynologyIOException(e.getMessage());
        }

        LOG.debug("performLogin=" + performLogin);
        LOG.debug(httpPost.getURI());
        for (Header header : httpPost.getAllHeaders()) {
            LOG.debug("header=" + header);
        }
        LOG.debug(nameValuePairs);

        try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
            rc = processResponse(response);
        } catch (IOException e) {
            LOG.error(e);
            throw new SynologyIOException(e.getMessage());
        }
        return rc == null ? null : rc.trim();
    }

    private List<NameValuePair> fillParameters(SynologyAction action, NameValuePair[] additionalParameters) {
        List<NameValuePair> nvps = new ArrayList<>();
        if (action.getModule().getApi() != null) {
            nvps.add(new BasicNameValuePair("api", action.getModule().getApi()));
        }
        nvps.addAll(action.getParameters());
        Collections.addAll(nvps, additionalParameters);
        return nvps;
    }

    private String processResponse(CloseableHttpResponse response) throws SynologyIOException, IOException {
        String rc;

        HttpEntity entity = response.getEntity();
        String responseString = EntityUtils.toString(entity).trim();
        LOG.debug(responseString);
        validateSuccess(responseString);
        rc = responseString;
        // do something useful with the response body
        // and ensure it is fully consumed
        EntityUtils.consume(entity);

        return rc == null ? null : rc.trim();
    }
}
