package de.tkunkel.synology.audio;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.tkunkel.synology.common.Config;
import de.tkunkel.synology.common.SynologyAction;
import de.tkunkel.synology.common.SynologyWrapper;
import de.tkunkel.synology.common.data.SongList;
import de.tkunkel.synology.common.data.Status;
import de.tkunkel.synology.common.exceptions.SynologyIOException;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class AudioStationWrapper extends SynologyWrapper {
    private static Logger LOG = LogManager.getLogger(AudioStationWrapper.class);
    private ObjectMapper mapper = new ObjectMapper(); // create once, reuse

    public AudioStationWrapper(Config config) {
        super(config);
    }

    /**
     * Switch to next title in the current playlist.
     */
    public void next() throws SynologyIOException {
        LOG.entry();
        executeCommand(SynologyAction.AUDIOSTATION_NEXT);
        LOG.exit();
    }

    /**
     * Switch to previous title in the current playlist.
     */
    public void previous() throws SynologyIOException {
        LOG.entry();
        executeCommand(SynologyAction.AUDIOSTATION_PREVIOUS);
        LOG.exit();
    }

    /**
     * Pause the current playback.
     */
    public void pause() throws SynologyIOException {
        LOG.entry();
        executeCommand(SynologyAction.AUDIOSTATION_PAUSE);
        LOG.exit();
    }

    /**
     * Start the playback of the selected title. Does nothing if already active playback.
     */
    public void play() throws SynologyIOException {
        LOG.entry();
        executeCommand(SynologyAction.AUDIOSTATION_PLAY);
        LOG.exit();
    }

    /**
     * Stops the current playback. Does nothing if already stopped.
     */
    public void stop() throws SynologyIOException {
        LOG.entry();
        executeCommand(SynologyAction.AUDIOSTATION_STOP);
        LOG.exit();
    }

    /**
     * Requests the current played song with some detailed information from the Synology.
     */
    public Status status() throws SynologyIOException, IOException {
        LOG.entry();
        String result = executeCommand(SynologyAction.AUDIOSTATION_STATUS);

        JsonNode readTree = mapper.readTree(result);
        JsonNode data = readTree.get("data");
        Status status = mapper.treeToValue(data, Status.class);

        LOG.debug(status);
        LOG.exit();

        return status;
    }

    public SongList getSongList(int limit, String library, SongSortBy songSortBy) throws SynologyIOException, IOException {
        LOG.entry();
        String result = executeCommand(SynologyAction.AUDIOSTATION_SONGLIST
                , new BasicNameValuePair("limit", String.valueOf(limit))
                , new BasicNameValuePair("library", library)
                , new BasicNameValuePair("sort_by", songSortBy.getCommandName())
        );

        JsonNode readTree = mapper.readTree(result);
        JsonNode data = readTree.get("data");
        SongList songList = mapper.treeToValue(data, SongList.class);


        LOG.debug("songList=" + songList);
        LOG.exit();

        return songList;
    }

    public SongList getCurrentPlayList(SongSortBy songSortBy) throws SynologyIOException, IOException {
        LOG.entry();
        String result = executeCommand(SynologyAction.AUDIOSTATION_GET_PLAYLIST
                , new BasicNameValuePair("sort_direction", songSortBy.getCommandName())
        );

        JsonNode readTree = mapper.readTree(result);
        JsonNode data = readTree.get("data");
        SongList songList = mapper.treeToValue(data, SongList.class);

        LOG.debug("songList=" + songList);
        LOG.exit();

        return songList;
    }

    /**
     * Adds the given song-ids to the playlist. Doesn't check, if they are already in the list.
     */
    public void addToPlaylist(String library, String... songIds) throws SynologyIOException {
        //TODO add parameter to prevent duplicates, maybe as separate method
        LOG.entry();
        String result = executeCommand(SynologyAction.AUDIOSTATION_UPDATE_PLAYLIST
                , new BasicNameValuePair("library", library)
                , new BasicNameValuePair("songs", String.join(",", songIds))
        );

        LOG.debug(result);
        LOG.exit();
    }

    /**
     * Deletes the first <i>limit</i> entries of the current playlist
     */
    public void clearPlaylist(int limit) throws SynologyIOException {
        LOG.entry();
        LOG.debug("limit=" + limit);
        String result = executeCommand(SynologyAction.AUDIOSTATION_CLEAR_PLAYLIST
                , new BasicNameValuePair("limit", String.valueOf(limit))
        );

        LOG.debug("result=" + result);
        LOG.exit();
    }

    /**
     * Sets the volume to the given value.<br/>Note: negative values are set to 0 and values above 100 are set to 100 by the synology box.
     */
    public void setVolume(int volume) throws SynologyIOException, IOException {
        LOG.entry();
        executeCommand(SynologyAction.AUDIOSTATION_SET_VOLUME
                , new BasicNameValuePair("value", String.valueOf(volume))
        );

        LOG.exit();
    }

    public int getVolume() throws SynologyIOException, IOException {
        LOG.entry();
        String result = executeCommand(SynologyAction.AUDIOSTATION_GET_VOLUME);

        JsonNode readTree = mapper.readTree(result);
        JsonNode data = readTree.get("data");
        JsonNode volume = data.get("volume");

        LOG.debug("volume=" + volume);
        LOG.exit();

        return volume.asInt();
    }

}
