package de.tkunkel.synology.auth;

import de.tkunkel.synology.LocalSynologyTestbase;
import de.tkunkel.synology.common.SessionManager;
import de.tkunkel.synology.common.SynologyModule;
import de.tkunkel.synology.common.data.ActiveSessions;
import de.tkunkel.synology.common.data.LoginKey;
import de.tkunkel.synology.common.exceptions.NotInitializedException;
import de.tkunkel.synology.common.exceptions.SynologyIOException;
import de.tkunkel.synology.entry.EntryRequest;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class AuthWrapperTest extends LocalSynologyTestbase {

    @Test
    public void loginWithUnknownId() throws SynologyIOException, IOException, NotInitializedException {
        SessionManager.createInstance(configAudiostation);
        AuthWrapper authWrapper = new AuthWrapper(configAudiostation);
        String sessionId = "unknown";
        authWrapper.logout(sessionId);
    }

    @Test
    public void loginAndOut() throws SynologyIOException, IOException, NotInitializedException {
        SessionManager.createInstance(configAudiostation);
        AuthWrapper authWrapper = new AuthWrapper(configAudiostation);
        String sessionId = authWrapper.login(SynologyModule.AUDIOSTATION_REMOTEPLAYER);
        authWrapper.logout(sessionId);
    }

    @Test
    public void testRequestKey() throws SynologyIOException, IOException {
        AuthWrapper authWrapper = new AuthWrapper(configAudiostation);
        LoginKey key = authWrapper.requestKey();

        Assert.assertNotNull(key);
        Assert.assertNotNull(key.getCipherkey());
        Assert.assertNotNull(key.getPublicKey());
        Assert.assertNotNull(key.getCiphertoken());
    }

    @Test
    public void testListActiveSessions() throws SynologyIOException, IOException {
        SessionManager.createInstance(configSystem);
        EntryRequest entryRequest = new EntryRequest(configSystem);
        ActiveSessions activeSessions = entryRequest.listActiveSessions();

        Assert.assertNotNull(activeSessions);
    }

}