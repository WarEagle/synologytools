package de.tkunkel.synology.common.data;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "command",
        "cpu",
        "mem",
        "pid",
        "status"
})
public class Process {

    @JsonProperty("command")
    private String command;
    @JsonProperty("cpu")
    private Integer cpu;
    @JsonProperty("mem")
    private Integer mem;
    @JsonProperty("pid")
    private Integer pid;
    @JsonProperty("status")
    private String status;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The command
     */
    @JsonProperty("command")
    public String getCommand() {
        return command;
    }

    /**
     *
     * @param command
     * The command
     */
    @JsonProperty("command")
    public void setCommand(String command) {
        this.command = command;
    }

    /**
     *
     * @return
     * The cpu
     */
    @JsonProperty("cpu")
    public Integer getCpu() {
        return cpu;
    }

    /**
     *
     * @param cpu
     * The cpu
     */
    @JsonProperty("cpu")
    public void setCpu(Integer cpu) {
        this.cpu = cpu;
    }

    /**
     *
     * @return
     * The mem
     */
    @JsonProperty("mem")
    public Integer getMem() {
        return mem;
    }

    /**
     *
     * @param mem
     * The mem
     */
    @JsonProperty("mem")
    public void setMem(Integer mem) {
        this.mem = mem;
    }

    /**
     *
     * @return
     * The pid
     */
    @JsonProperty("pid")
    public Integer getPid() {
        return pid;
    }

    /**
     *
     * @param pid
     * The pid
     */
    @JsonProperty("pid")
    public void setPid(Integer pid) {
        this.pid = pid;
    }

    /**
     *
     * @return
     * The status
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(command).append(cpu).append(mem).append(pid).append(status).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Process) == false) {
            return false;
        }
        Process rhs = ((Process) other);
        return new EqualsBuilder().append(command, rhs.command).append(cpu, rhs.cpu).append(mem, rhs.mem).append(pid, rhs.pid).append(status, rhs.status).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}