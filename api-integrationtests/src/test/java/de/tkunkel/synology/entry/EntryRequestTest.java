package de.tkunkel.synology.entry;

import de.tkunkel.synology.LocalSynologyTestbase;
import de.tkunkel.synology.common.SessionManager;
import de.tkunkel.synology.common.data.ConnectionsList;
import de.tkunkel.synology.common.data.Result;
import de.tkunkel.synology.common.exceptions.SynologyIOException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class EntryRequestTest extends LocalSynologyTestbase {

    @Test
    public void callTestListNetworkdevices() throws SynologyIOException, IOException {
        SessionManager.createInstance(configSystem);
        EntryRequest entryRequest = new EntryRequest(configSystem);
        ConnectionsList connectionsList = entryRequest.listNetworkDevices();

        Assert.assertNotNull(connectionsList);
        Assert.assertNotNull(connectionsList.getResult());
        Assert.assertEquals(2, connectionsList.getResult().size());
    }

    @Test
    public void callTestGetInfoForType() throws SynologyIOException, IOException {
        SessionManager.createInstance(configSystem);
        EntryRequest entryRequest = new EntryRequest(configSystem);
        NetworkInfo infoForType = entryRequest.getInfoForType(NetworkType.SYNO_Core_Network_VPN_PPTP);
        Assert.assertNotNull(infoForType);
        Assert.assertEquals(NetworkType.SYNO_Core_Network_VPN_PPTP, infoForType.getType());
        Assert.assertNotNull(infoForType.getData());
        Assert.assertNotNull(infoForType.getMethod());
        Assert.assertNotNull(infoForType.getVersion());
    }

    @Test
    public void validateNetworkTypes() throws SynologyIOException, IOException {
        SessionManager.createInstance(configSystem);
        EntryRequest entryRequest = new EntryRequest(configSystem);
        ConnectionsList connectionsList = entryRequest.listNetworkDevices();

        Assert.assertNotNull(connectionsList);
        Assert.assertNotNull(connectionsList.getResult());
        Assert.assertEquals(2, connectionsList.getResult().size());

        Result result1 = connectionsList.getResult().get(0);
        validate(result1);
        Result result2 = connectionsList.getResult().get(1);
        validate(result2);
    }

    private void validate(Result result) {
        if (result.getApi().equals("SYNO.Entry.Request")) {
            validateSYNOEntryRequest(result);
        } else if (result.getApi().equals("SYNO.CloudSync")) {
            validateSYNOCloudSync(result);
        } else {
            Assert.fail("Unexpected Api: " + result.getApi());
        }
    }

    private void validateSYNOCloudSync(Result result) {
        //TODO
    }

    @SuppressWarnings("unchecked")
    private void validateSYNOEntryRequest(Result result) {
        Assert.assertEquals(2, result.getData().getAdditionalProperties().size());
        // System.out.println(result.getData().getAdditionalProperties().get("result"));
        // System.out.println(result.getData().getAdditionalProperties().get("has_fail"));

        ArrayList<HashMap> resultList = (ArrayList<HashMap>) result.getData().getAdditionalProperties().get("result");
        // Boolean resultHasFail = Boolean.valueOf(String.valueOf(result.getData().getAdditionalProperties().get("has_fail")));

        for (HashMap map : resultList) {
            System.out.println(map);
            NetworkType api = NetworkType.fromString(String.valueOf(map.get("api")));
            Assert.assertNotNull(api);
        }
    }
}