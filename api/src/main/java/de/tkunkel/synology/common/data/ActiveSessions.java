package de.tkunkel.synology.common.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "items",
        "systime",
        "total"
})
public class ActiveSessions {

    @JsonProperty("items")
    private List<ActiveSession> items = new ArrayList<ActiveSession>();
    @JsonProperty("systime")
    private String systime;
    @JsonProperty("total")
    private Integer total;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The items
     */
    @JsonProperty("items")
    public List<ActiveSession> getItems() {
        return items;
    }

    /**
     *
     * @param items
     * The items
     */
    @JsonProperty("items")
    public void setItems(List<ActiveSession> items) {
        this.items = items;
    }

    /**
     *
     * @return
     * The systime
     */
    @JsonProperty("systime")
    public String getSystime() {
        return systime;
    }

    /**
     *
     * @param systime
     * The systime
     */
    @JsonProperty("systime")
    public void setSystime(String systime) {
        this.systime = systime;
    }

    /**
     *
     * @return
     * The total
     */
    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    /**
     *
     * @param total
     * The total
     */
    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(items).append(systime).append(total).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ActiveSessions) == false) {
            return false;
        }
        ActiveSessions rhs = ((ActiveSessions) other);
        return new EqualsBuilder().append(items, rhs.items).append(systime, rhs.systime).append(total, rhs.total).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}