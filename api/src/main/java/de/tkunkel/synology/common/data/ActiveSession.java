package de.tkunkel.synology.common.data;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "can_be_kicked",
        "descr",
        "from",
        "pid",
        "time",
        "type",
        "user_can_be_disabled",
        "who"
})
public class ActiveSession {

    @JsonProperty("can_be_kicked")
    private Boolean canBeKicked;
    @JsonProperty("descr")
    private String descr;
    @JsonProperty("from")
    private String from;
    @JsonProperty("pid")
    private Integer pid;
    @JsonProperty("time")
    private String time;
    @JsonProperty("type")
    private String type;
    @JsonProperty("user_can_be_disabled")
    private Boolean userCanBeDisabled;
    @JsonProperty("who")
    private String who;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The canBeKicked
     */
    @JsonProperty("can_be_kicked")
    public Boolean getCanBeKicked() {
        return canBeKicked;
    }

    /**
     *
     * @param canBeKicked
     * The can_be_kicked
     */
    @JsonProperty("can_be_kicked")
    public void setCanBeKicked(Boolean canBeKicked) {
        this.canBeKicked = canBeKicked;
    }

    /**
     *
     * @return
     * The descr
     */
    @JsonProperty("descr")
    public String getDescr() {
        return descr;
    }

    /**
     *
     * @param descr
     * The descr
     */
    @JsonProperty("descr")
    public void setDescr(String descr) {
        this.descr = descr;
    }

    /**
     *
     * @return
     * The from
     */
    @JsonProperty("from")
    public String getFrom() {
        return from;
    }

    /**
     *
     * @param from
     * The from
     */
    @JsonProperty("from")
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     *
     * @return
     * The pid
     */
    @JsonProperty("pid")
    public Integer getPid() {
        return pid;
    }

    /**
     *
     * @param pid
     * The pid
     */
    @JsonProperty("pid")
    public void setPid(Integer pid) {
        this.pid = pid;
    }

    /**
     *
     * @return
     * The time
     */
    @JsonProperty("time")
    public String getTime() {
        return time;
    }

    /**
     *
     * @param time
     * The time
     */
    @JsonProperty("time")
    public void setTime(String time) {
        this.time = time;
    }

    /**
     *
     * @return
     * The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The userCanBeDisabled
     */
    @JsonProperty("user_can_be_disabled")
    public Boolean getUserCanBeDisabled() {
        return userCanBeDisabled;
    }

    /**
     *
     * @param userCanBeDisabled
     * The user_can_be_disabled
     */
    @JsonProperty("user_can_be_disabled")
    public void setUserCanBeDisabled(Boolean userCanBeDisabled) {
        this.userCanBeDisabled = userCanBeDisabled;
    }

    /**
     *
     * @return
     * The who
     */
    @JsonProperty("who")
    public String getWho() {
        return who;
    }

    /**
     *
     * @param who
     * The who
     */
    @JsonProperty("who")
    public void setWho(String who) {
        this.who = who;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(canBeKicked).append(descr).append(from).append(pid).append(time).append(type).append(userCanBeDisabled).append(who).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ActiveSession) == false) {
            return false;
        }
        ActiveSession rhs = ((ActiveSession) other);
        return new EqualsBuilder().append(canBeKicked, rhs.canBeKicked).append(descr, rhs.descr).append(from, rhs.from).append(pid, rhs.pid).append(time, rhs.time).append(type, rhs.type).append(userCanBeDisabled, rhs.userCanBeDisabled).append(who, rhs.who).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
