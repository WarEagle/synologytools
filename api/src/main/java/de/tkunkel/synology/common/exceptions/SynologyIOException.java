package de.tkunkel.synology.common.exceptions;

public class SynologyIOException extends Exception{
    public SynologyIOException(String message) {
        super(message);
    }
}
