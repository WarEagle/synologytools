package de.tkunkel.synology.vpnwatcher;

import de.tkunkel.synology.common.Config;
import de.tkunkel.synology.common.SessionManager;
import de.tkunkel.synology.common.exceptions.SynologyIOException;
import de.tkunkel.synology.entry.EntryRequest;
import de.tkunkel.synology.entry.NetworkInfo;
import de.tkunkel.synology.entry.NetworkType;

import java.awt.*;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class UpdateThread implements Runnable {

    private String networkName;
    private NetworkType networkType;
    private TrayIcon trayIcon;
    private Image imageOk;
    private Image imageError;
    private EntryRequest entryRequest;

    public UpdateThread(Config config, TrayIcon trayIcon, Image imageOk, Image imageError, String networkName, NetworkType networkType) {
        this.trayIcon = trayIcon;
        this.imageOk = imageOk;
        this.imageError = imageError;
        this.networkName = networkName;
        this.networkType = networkType;

        SessionManager.createInstance(config);
        entryRequest = new EntryRequest(config);

    }

    @Override
    public void run() {
        boolean oldConnectionStatus = false;
        while (true) {

            boolean isConnected = checkIfVpnIsConnected();
            trayIcon.setImage(isConnected ? imageOk : imageError);

            if (oldConnectionStatus == true && isConnected == false) {
                messageDisconnection();
            }
            oldConnectionStatus = isConnected;

            sleep();
        }
    }

    private void messageDisconnection() {
        EventQueue.invokeLater(() -> trayIcon.displayMessage("Disconnection", "Connection got terminated", TrayIcon.MessageType.ERROR));
    }

    public void sleep() {
        try {
            //Thread.sleep(TimeUnit.SECONDS.toMillis(1));
            Thread.sleep(TimeUnit.MINUTES.toMillis(5));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private boolean checkIfVpnIsConnected() {
        boolean rc = false;
        try {
            if (entryRequest == null){
                return false;
            }
            NetworkInfo infoForType = entryRequest.getInfoForType(networkType);
            String status = String.valueOf(infoForType.getData().getData().get("status"));
            String confName = String.valueOf(infoForType.getData().getData().get("confname"));

            if (networkName.equalsIgnoreCase(confName) && "connected".equalsIgnoreCase(status)) {
                rc = true;
            }

        } catch (SynologyIOException | IOException e) {
            e.printStackTrace();
            rc = false;
        }

        return rc;
    }
}
