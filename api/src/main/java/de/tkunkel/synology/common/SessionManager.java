package de.tkunkel.synology.common;

import de.tkunkel.synology.auth.AuthWrapper;
import de.tkunkel.synology.common.exceptions.NotInitializedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class SessionManager {
    private static Config config;
    private static SessionManager INSTANCE = null;
    private Map<SynologyModule, String> sessionIds = new HashMap<>(SynologyModule.values().length);
    private static Logger LOG = LogManager.getLogger(SessionManager.class);

    /**
     * Returns the current sessionmanager.
     * @return current sessionmanager
     * @throws NotInitializedException when no createInstance was called before this
     */
    public static SessionManager getInstance() throws NotInitializedException {
        if (INSTANCE == null) {
            throw new NotInitializedException("SessionManager");
        }
        return INSTANCE;
    }

    public static SessionManager createInstance(Config config) {
        SessionManager.config = config;
        INSTANCE = new SessionManager();

        return INSTANCE;

    }

    public String getSessionIdFor(SynologyModule module) {
        return getSessionIdFor(module, true);
    }

    public String getSessionIdFor(SynologyModule module, boolean createIfNotLogin) {
        if (!sessionIds.containsKey(module) && !createIfNotLogin) {
            return null;
        }else if (!sessionIds.containsKey(module) && createIfNotLogin){
            AuthWrapper authWrapper = new AuthWrapper(config);
            try {
                String sessionId = authWrapper.login(module);
                sessionIds.put(module, sessionId);
            } catch (Exception e) {
                LOG.error(e);
                e.printStackTrace();
            }
        }
        //TODO check if session is valid
        return sessionIds.get(module);
    }

    public void dropSessionIdFor(SynologyModule module) {
        sessionIds.remove(module);
    }
}
