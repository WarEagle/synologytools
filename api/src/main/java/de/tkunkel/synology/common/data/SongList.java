package de.tkunkel.synology.common.data;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "offset",
        "songs",
        "total"
})
public class SongList {

    @JsonProperty("offset")
    private Integer offset;
    @JsonProperty("songs")
    private List<Song> songs = new ArrayList<Song>();
    @JsonProperty("total")
    private Integer total;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The offset
     */
    @JsonProperty("offset")
    public Integer getOffset() {
        return offset;
    }

    /**
     * @param offset The offset
     */
    @JsonProperty("offset")
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    /**
     * @return The songs
     */
    @JsonProperty("songs")
    public List<Song> getSongs() {
        return songs;
    }

    /**
     * @param songs The songs
     */
    @JsonProperty("songs")
    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    /**
     * @return The total
     */
    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    /**
     * @param total The total
     */
    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(offset).append(songs).append(total).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SongList) == false) {
            return false;
        }
        SongList rhs = ((SongList) other);
        return new EqualsBuilder().append(offset, rhs.offset).append(songs, rhs.songs).append(total, rhs.total).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}