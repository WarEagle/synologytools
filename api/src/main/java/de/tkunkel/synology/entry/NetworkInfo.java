package de.tkunkel.synology.entry;

public class NetworkInfo {
    private NetworkType type;
    private String method;
    private int version;
    private boolean success;
    private NetworkInfoData data = new NetworkInfoData();

    public NetworkType getType() {
        return type;
    }

    public String getMethod() {
        return method;
    }

    public int getVersion() {
        return version;
    }

    public boolean isSuccess() {
        return success;
    }

    public NetworkInfoData getData() {
        return data;
    }

    public void setType(NetworkType type) {
        this.type = type;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
