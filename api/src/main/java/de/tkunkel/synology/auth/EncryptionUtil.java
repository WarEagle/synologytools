package de.tkunkel.synology.auth;

import sun.security.rsa.RSAPublicKeyImpl;

import javax.crypto.Cipher;
import java.io.*;
import java.security.*;

/**
 * @author JavaDigest
 */
public class EncryptionUtil {

    /**
     * String to hold name of the encryption algorithm.
     */
    public static final String ALGORITHM = "RSA";

    /**
     * Encrypt the plain text using public key.
     *
     * @param text : original plain text
     * @param key  :The public key
     * @return Encrypted text
     * @throws Exception
     */
    public static byte[] encrypt(String text, PublicKey key) {
        byte[] cipherText = null;
        try {
            // get an RSA cipher object and print the provider
            final Cipher cipher = Cipher.getInstance(ALGORITHM);
            // encrypt the plain text using the public key
            cipher.init(Cipher.ENCRYPT_MODE, key);
            cipherText = cipher.doFinal(text.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cipherText;
    }

    /**
     * Test the EncryptionUtil
     */
    public static void main(String[] args) throws InvalidKeyException {
            final String originalText = "Text to be encrypted ";

            // Encrypt the string using the public key
            PublicKey publicKey = new RSAPublicKeyImpl("".getBytes());
            final byte[] cipherText = encrypt(originalText, publicKey);

            System.out.println("Original Text: " + originalText);
            System.out.println("Encrypted Text: " + cipherText.toString());
    }
}