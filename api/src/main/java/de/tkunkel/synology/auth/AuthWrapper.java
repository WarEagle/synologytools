package de.tkunkel.synology.auth;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.tkunkel.synology.common.*;
import de.tkunkel.synology.common.data.LoginKey;
import de.tkunkel.synology.common.exceptions.NotInitializedException;
import de.tkunkel.synology.common.exceptions.SynologyIOException;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class AuthWrapper extends SynologyWrapper {
    private static Logger LOG = LogManager.getLogger(AuthWrapper.class);
    private ObjectMapper mapper = new ObjectMapper(); // create once, reuse

    public AuthWrapper(Config config) {
        super(config);
        this.config = config;
    }

    public void logout(String sessionId) throws SynologyIOException, IOException, NotInitializedException {
        LOG.entry();

        //executeCommand(SynologyAction.AUTH_LOGOUT);
        executeCommandSpecialLogout(SynologyAction.AUTH_LOGOUT, sessionId);

        LOG.exit();
    }

//TODO create logout nased on module
//    public void logout(SynologyModule module) throws SynologyIOException, IOException, NotInitializedException {
//        LOG.entry();
//
//        executeCommandWithoutLogin(SynologyAction.AUTH_LOGOUT
//                , new BasicNameValuePair("Cookie", "stay_login=1; id=" + SessionManager.getInstance().getSessionIdFor(module, false))
//        );
//        SessionManager.getInstance().dropSessionIdFor(module);
//
//        LOG.exit();
//    }
//
    public String login(SynologyModule module) throws SynologyIOException, IOException {
        LOG.entry();
        String rc;
        String responseString = executeCommandWithoutLogin(SynologyAction.AUTH_LOGIN
                , new BasicNameValuePair("session", module.getModuleName())
                , new BasicNameValuePair("account", config.getUsername())
                , new BasicNameValuePair("passwd", config.getPassword())
                , new BasicNameValuePair("format", "cookie")
        );

        JsonNode readTree = mapper.readTree(responseString);
        JsonNode data = readTree.get("data");
        if (data == null) {
            //TODO test for errors
            throw new SynologyIOException("ConnectionsList not found in " + responseString);
        }
        JsonNode sid = data.get("sid");
        //LOG.debug("sid=" + sid);
        rc = sid.asText();
        LOG.debug("rc=" + rc);
        LOG.exit();

        return rc;
    }

//    public String login2(SynologyModule module) throws SynologyIOException {
//        String rc = null;
//        //looks like it must be HttpGet
//
//        HttpGet httpGet = new HttpGet(config.getLoginUrl(config.getUsername(), config.getPassword(), module.getModuleName()));
//
//        CloseableHttpClient httpclient = HttpClients.createDefault();
//        CloseableHttpResponse response2 = httpclient.execute(httpGet);
//
//        try {
//            HttpEntity entity2 = response2.getEntity();
//            String responseString = EntityUtils.toString(entity2);
//            validateSuccess(responseString);
//            LOG.debug(responseString.trim());
//
//            JsonNode readTree = mapper.readTree(responseString);
//            JsonNode data = readTree.get("data");
//            if (data == null) {
//                //TODO test for errors
//                throw new Exception("ConnectionsList not found in " + responseString);
//            }
//            JsonNode sid = data.get("sid");
//            LOG.debug(sid);
//            rc = sid.asText();
//
//            // do something useful with the response body
//            // and ensure it is fully consumed
//            EntityUtils.consume(entity2);
//        } finally {
//            response2.close();
//        }
//        return rc;
//    }

    public void login2() throws SynologyIOException {
        String rc = null;
        LOG.entry();

        String responseString = executeCommandWithoutLogin(SynologyAction.LOGIN_LOGIN
                , new BasicNameValuePair("OTPcode", "")
                //TODO
                , new BasicNameValuePair("__cIpHeRtExT", "%7B%22rsa%22%3A%22qvCpl0EvcCeZdvoZyY6Q7KxfcX7gWCT5NoNMBZBzlUzTpxmkOfxp9KmLAZ%2FIXfcpshtxbEUj7%2F7Ff52%2BnctuOpk7XZM5my%2FpkvjzlyCD0nQx72GqabqB%2BDLLff37ZtMwYb0VtfuxLej5uYQUAzDwNxut2yud70EMIfiUjvPDVyq695hiHf8zYN4oAmHtgaGez6iFy1iQO5PYinuvdrqaW9jhJaBdGlRarMB%2FBWbz7aS1S%2FzSnN7k7wvIq%2F0lB4i7ieuk2FnHUKR%2BMG1vtzauG43IT5fwMsXdGuraBjOSvn2K6aRgiKMoWY4V32grqMf%2Bxb%2Fvmkmyw7ac4HwEWOs3vGdy4cEzYLVnCtI%2FqthhpaF5lwhB9zgm0Mg2LIFJQGZPJytc0zhqoo22%2FfAjNNuu1u225ztTNCrlVGgSrCPLMV6TbqhpvUoTJIT8GPRoAL%2Bf0knH2YgDI1vfCui9pn1kZI6bZsZxPJVIgWQm72UtXul42jQ3hNajHaY%2BsohQ1SOCWZF2a0MQiV4PYprLUHeBzE1ek87%2Fz4T05JATBtX%2BYrTszH1gIbnlTjjIxNdYomCeMVqjqKhN7UpWSuzJVuMr6RrlZHGFJxyI3I0OX6ISVQ7LLynz%2FFSSERALAwOY30SxDRU69EWkSIEzzM6WfHUy5FQQrTsHLn3JLQwQypS55jk%3D%22%2C%22aes%22%3A%22U2FsdGVkX1%2Fy0YlJtgKVhMTEuL8XhqHD1v%2FiHSgbfW033ciJE%2BEKzJFM48vTe%2F6%2Bl4%2BntOdr%2FUTgY9Qg28Gyvra2K7Tj3aD2T39s%2Bd2Jdalb0pjBCIOwyjT2uw%2FdpW1eMsDoyTIImKxsinTkNUS7kCohQqm5Vfc%2BOCXqGwrtjDUv%2BADXnrwWWgaofRPfyuwKtS1%2FjYAPcF%2BwPOwpwLreow%3D%3D%22%7D")
                , new BasicNameValuePair("__cIpHeRtExT", "{\"rsa\":\"RjVAo08LDaa8FZjePCKrarqWai0yEiYGAmAlhsE0F+3MykuljtmK25YiOLfe+cnefdkd+GqLQRneg5ynFALOiT4K6JWdGF6bQheuoZovV9AxxvzAjcm/ExsdauahQeXVDwB2Yp5gd0wsW8cWcOz1l9gpZ17U5/KjehiTHkPo9RfM1rpT3wXwiJgpWIvKM9oiaxdgZi0idgIz1DAa09E+wQz5ZqO4jAfn9hOs06IkllZrBIp6a4UjtvK+Ox3HjY3wpRBHOIJOFmFNvWTwYuLmQ6Jm9cVTwcYqWiAbJAeLS/QlOfMrqZ3QlWEMoZ6EU83V21DomrWOiFoPs3kBv7dcsKq3bvlcs79XRV7UpnipXAlDkdn7keIHVCHQR/vahTOAIOumCtyy4HTqYrhsEQZj449q0+HmGVrixsmWJAA+XXCxxL1ZPvPfTll6IICcqTI4QT2ElsvgoMSBmTboFACDhGKLQnKSw8P9TF3NyWqTds/CH0tZIxhdvEuBZMsd8BFMOi5jn1pS4KPE/Fe5wEdt95P6G/ImJ7zRhqMWb3j8/mOETVH5tu1XbcEhYX2lQamWN7eIPf3Kz6N/VoAPCbEVrcumahx39UVJYsaPpHvBFI1ycMaPeqBGWhiMhVWIoEmk2r085DwvHMDEP2OkzyVR5IjJ1XasP5uuJJfmNRaZGB8=\",\"aes\":\"U2FsdGVkX19gdbn8dLwclz6pQDBj2sCmLKAbI2GHPG1IE5u3u2LYoux0RXifEUyqnBIhdGdOYwp5udIif1YzHcremu0MUtaZ4pADTMz98tPcemH5aOFOGgnzLlLI1IE60Zu/LXl1kWnR3zlOUjf7mSNWTOXhNI0dRQ+KN2ONB+w49wQlKtKy5UtQoN4R0HL7pIlof6tchs15KhNyfhjnvQ==\"}")
                //TODO
                //, new BasicNameValuePair("client_time", "1451794388")
                //TODO
                //, new BasicNameValuePair("isIframeLogin", "yes")
        );

        LOG.debug("responseString=" + responseString);
        LOG.exit();
    }

    public LoginKey requestKey() throws SynologyIOException, IOException {
        LOG.entry();

        String responseString = executeCommandWithoutLogin(SynologyAction.ENCRYPTION_GET_KEY);

        LOG.debug("responseString=" + responseString);
        JsonNode readTree = mapper.readTree(responseString);
        JsonNode data = readTree.get("data");
        LoginKey keyData = mapper.treeToValue(data, LoginKey.class);

        return LOG.exit(keyData);
    }

}
